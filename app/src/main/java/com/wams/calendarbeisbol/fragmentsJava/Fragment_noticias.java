package com.wams.calendarbeisbol.fragmentsJava;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.wams.calendarbeisbol.Adapters.Adapter_noticias;
import com.wams.calendarbeisbol.R;

/**
 * Created by WilliamO on 9/1/2016.
 */
public class Fragment_noticias extends Fragment {
    Adapter_noticias mAdapterTable;
    ListView listViewNoticias;
    ImageView image_noticias_share;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View V = inflater.inflate(R.layout.fragment_noticias, container, false);
        View V2 = inflater.inflate(R.layout.items_listview_noticias, container, false);
        listViewNoticias = (ListView) V.findViewById(R.id.listView_noticias);
        image_noticias_share = (ImageView) V2.findViewById(R.id.imageView_share);


        mAdapterTable = new Adapter_noticias(getActivity(),getContext());
        listViewNoticias.setAdapter(mAdapterTable);
        return V;
    }
}

package com.wams.calendarbeisbol.fragmentsJava;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wams.calendarbeisbol.Adapters.Adapter_tabs_fechas;
import com.wams.calendarbeisbol.Conection.ConexionHTTP;
import com.wams.calendarbeisbol.Conection.DataCallback;
import com.wams.calendarbeisbol.FragmentCalendar;
import com.wams.calendarbeisbol.Libs.AlmacenamientoLocal;
import com.wams.calendarbeisbol.Libs.Configuracion;
import com.wams.calendarbeisbol.Libs.ConnectionDetector;
import com.wams.calendarbeisbol.Parse.Parse_calendar_dates;
import com.wams.calendarbeisbol.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class Fragment_Scores extends Fragment  implements DataCallback {
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public JSONArray jsondata;
    public String[] data_dates={""};
    Parse_calendar_dates parseM;
    ConnectionDetector conexion;
    private AlmacenamientoLocal cache;
    Adapter_tabs_fechas mAdapterTable;
    ArrayList<String>fechasScores = new ArrayList<>();
    int indexTabCalendar=0;
    Calendar calendar;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_tabs_date, null);
       /* View v = inflater.inflate(R.layout.app_bar_main,null);
        Toolbar tb = (Toolbar)v.findViewById(R.id.toolbar);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView title = (TextView) tb.findViewById(R.id.toolbar_title);
        title.setText("Resultados"); */


        Bundle b = getActivity().getIntent().getExtras();
        if(b!=null){
           indexTabCalendar= b.getInt("dateCalendario");
        }
        parseM = new Parse_calendar_dates();
        conexion = new ConnectionDetector(getActivity());
        cache = new AlmacenamientoLocal(getActivity());
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        if (conexion.hasInternet(getActivity())) {
            // ActionBarPullToRefresh.from(getActivity()).allChildrenArePullable().listener(this).setup(mPullToRefreshLayout);
            try {
                ConexionHTTP.getWebData(Configuracion.JSON_CALENDAR,this);
            } catch (Exception e) {
                e.printStackTrace();
                // Analytics.logError("JSONget JuegosFragment", e.getMessage(), e.getCause());
            }
         viewPager.setAdapter(new Adapter_tabs_fechas(getChildFragmentManager(), data_dates,jsondata));
        }else {
            //progress = (ProgressBar) V.findViewById(R.id.progressBar1);
            //progress.setVisibility(View.VISIBLE);
            if (cache.getVariablePermanente("Score") == null || cache.getVariablePermanente("Score").equals("")) {
                Toast.makeText(getActivity(), getResources().getString(R.string.toast2), Toast.LENGTH_LONG).show();
            } else {
                try {
                    jsondata = new JSONArray(cache.getVariablePermanente("Score"));
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                cargaContenido();
            }
        }
        return view;
    }

    /*class runClass implements Runnable {
        public void run()
        {
            Fragment_Scores.tabLayout.setupWithViewPager(Fragment_Scores.viewPager);
            viewPager.setCurrentItem(indexTabCalendar);
            Fragment_Scores.tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.calendario_menu:
                Intent intent = new Intent(getActivity(), FragmentCalendar.class);
                intent.putExtra("fechasScores",fechasScores);
                startActivity(intent);
                break;
        }
        return true;
    }

    public void cargaContenido(){
        calendar = Calendar.getInstance();
        String stringauxMes="";
        String stringauxDia="";
        int dia  = 05/*calendar.get(Calendar.DAY_OF_MONTH)*/;
        int mes  = 10/*calendar.get(Calendar.MONTH)*/;
        int anio = 2015/*calendar.get(Calendar.YEAR)*/;
        if(dia <=9){
            stringauxDia = "0"+dia;
        }else{
            stringauxDia = Integer.toString(dia);
        }

        if(mes <9){
            int m =mes+1;
            stringauxMes = "0"+m;
        }else{
            int m = mes +1;
            stringauxMes = Integer.toString(m);
        }
        //String fechaCalendar = stringauxDia+"/"+ stringauxMes+"/"+anio;
        String fechaCalendar = "05/10/2015";

        fechasScores = parseM.parseDates(jsondata);
        data_dates = fechasScores.toArray(new String[fechasScores.size()]);
        viewPager.setAdapter(new Adapter_tabs_fechas(getChildFragmentManager(), data_dates,jsondata));

        Fragment_Scores.tabLayout.setupWithViewPager(Fragment_Scores.viewPager);

        if(Arrays.asList(data_dates).contains(fechaCalendar)) {
            int index = Arrays.asList(data_dates).indexOf(fechaCalendar);
            viewPager.setCurrentItem(index);
        }else if(!Arrays.asList(data_dates).contains(fechaCalendar)){
            if(indexTabCalendar==0){
                for(int i=1; i <=data_dates.length;i++){
                    String arrayDates[] = data_dates[i].split("/");
                    int d= Integer.parseInt(arrayDates[0]);
                    int m = Integer.parseInt(arrayDates[1]);
                    int a = Integer.parseInt(arrayDates[2]);
                    if(dia <d && mes <= m && anio <= a) {
                        viewPager.setCurrentItem(i);
                        break;
                    }
                }
            }
        }
        if(indexTabCalendar != 0){
            viewPager.setCurrentItem(indexTabCalendar);
            //indexTabCalendar=0;
        }
        Fragment_Scores.tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
    }
    @Override
    public void callback(String data) {
        if (!data.equals("") && data != null && data.indexOf("Exception") < 0) {
            try {
                jsondata = new JSONArray(data);
                cache.setVariablePermanente("Score", jsondata.toString());
                cargaContenido();
            } catch (Exception ex) {
                // Analytics.logError("JuegosFragment.callback()", ex.getMessage(), ex.getCause());
            }
        }
    }
}
package com.wams.calendarbeisbol.fragmentsJava;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wams.calendarbeisbol.R;

/**
 * Created by WilliamO on 9/2/2016.
 */
public class Fragment_estadisticas extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View V = inflater.inflate(R.layout.fragment_estadisticas, container, false);

       final TextView lbl_pitcheo = (TextView)V.findViewById(R.id.lbl_noticias_pitcheo);
        final TextView lbl_bateo= (TextView)V.findViewById(R.id.lbl_noticias_bateo);

       FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content_estadisticas, Fragment_estadisticas_tabs.newInstance(2));
        ft.commit();


        lbl_pitcheo.setTextColor(Color.WHITE);
        lbl_pitcheo.setBackgroundResource(R.color.rojo_beisbol);
        lbl_bateo.setTextColor(Color.RED);

          lbl_pitcheo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lbl_pitcheo.setTextColor(Color.WHITE);
                lbl_pitcheo.setBackgroundResource(R.color.rojo_beisbol);
                lbl_bateo.setTextColor(Color.RED);
                lbl_bateo.setBackgroundResource(R.color.blanco_beisbol_text);
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.content_estadisticas, Fragment_estadisticas_tabs.newInstance(1));
                ft.commit();
            }
        });
        lbl_bateo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lbl_pitcheo.setTextColor(Color.RED);
                lbl_pitcheo.setBackgroundResource(R.color.blanco_beisbol_text);
                lbl_bateo.setBackgroundResource(R.color.rojo_beisbol);
                lbl_bateo.setTextColor(Color.WHITE);
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.content_estadisticas, Fragment_estadisticas_tabs.newInstance(2));
                ft.commit();
            }
        });

       /* TabHost TbH = (TabHost) V.findViewById(R.id.tabHost); //llamamos al Tabhost
        TbH.setup();                                                         //lo activamos

        final TabHost.TabSpec tab1 = TbH.newTabSpec("Bateo");  //aspectos de cada Tab (pestaña)
        final TabHost.TabSpec tab2 = TbH.newTabSpec("Pitcheo");

        tab1.setIndicator("Bateo");    //qué queremos que aparezca en las pestañas
        tab1.setContent(R.id.ejemplo1); //definimos el id de cada Tab (pestaña)

        tab2.setIndicator("Pitcheo");
        tab2.setContent(R.id.ejemplo2);

        TbH.addTab(tab1); //añadimos los tabs ya programados
        TbH.addTab(tab2);

        TbH.setOnTabChangedListener(new TabHost.OnTabChangeListener(){
            @Override
            public void onTabChanged(String tabId) {
                if(tab1.equals(tabId)) {
                    //Change first image
                    Log.i("ivan","Has pulsado :" + tabId);
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(android.R.id.tabcontent, Fragment_estadisticas_tabs.newInstance(2));
                    ft.commit();
                }
                if(tab2.equals(tabId)) {
                    //chnage second image ...so on
                    Log.i("ivan","Has pulsado :" + tabId);
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(android.R.id.tabcontent, Fragment_estadisticas_tabs.newInstance(1));
                    ft.commit();
                }
            }});*/




        return V;
    }
}
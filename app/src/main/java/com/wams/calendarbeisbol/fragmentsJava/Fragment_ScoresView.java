package com.wams.calendarbeisbol.fragmentsJava;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wams.calendarbeisbol.Adapters.Adapter_scores;
import com.wams.calendarbeisbol.Conection.ConexionHTTP;
import com.wams.calendarbeisbol.Conection.DataCallback;
import com.wams.calendarbeisbol.Libs.AlmacenamientoLocal;
import com.wams.calendarbeisbol.Libs.Configuracion;
import com.wams.calendarbeisbol.Libs.ConnectionDetector;
import com.wams.calendarbeisbol.Parse.Parse_calendar_dates;
import com.wams.calendarbeisbol.PlaybyPlay_Activity;
import com.wams.calendarbeisbol.R;

import org.json.JSONArray;

/**
 * Created by WilliamO on 6/30/2016.
 */
public class Fragment_ScoresView extends Fragment implements DataCallback {
    private AlmacenamientoLocal cache;
    JSONArray jsondata;
    ConnectionDetector conexion;
    ListView listViewScores;
    Adapter_scores mAdapterTable;
    private SwipeRefreshLayout swipeContainer;
    public Parse_calendar_dates parseM;
    ProgressBar progress;
    public String fechas;
    JSONArray jsonscorecontent;
    Button  botonAgendar;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View V = inflater.inflate(R.layout.fragment_scoresview, container, false);
        View Vi = inflater.inflate(R.layout.items_listview_scores, container, false);

        setHasOptionsMenu(true);
        parseM = new Parse_calendar_dates();
        conexion = new ConnectionDetector(getActivity());
        cache = new AlmacenamientoLocal(getActivity());
        listViewScores = (ListView) V.findViewById(R.id.listViewScores);
        progress = (ProgressBar) V.findViewById(R.id.progressBarScores);
        botonAgendar = (Button)Vi.findViewById(R.id.buttonAgendar);
        swipeContainer = (SwipeRefreshLayout) V.findViewById(R.id.swipeContainerScoresView);
        progress.setVisibility(View.VISIBLE);

       /* botonAgendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            try{
            for (int i = 0; i <=jsonscorecontent.length(); i++) {
                HashMap<String, String> map = new HashMap();
                JSONObject d = jsonscorecontent.getJSONObject(i);
                estadioAgenda = d.getString(Parse_calendar_dates.TAG_ESTADIO);
                localagenda   = d.getString(Parse_calendar_dates.TAG_LOCAL);
                visitorAgenda = d.getString(Parse_calendar_dates.TAG_VISITANTE);
                horaAgenda   = d.getString(Parse_calendar_dates.TAG_HORA);
                fechaAgendar = d.getString(Parse_calendar_dates.TAG_FECHA);
                String fechaArray[] = fechaAgendar.split("/");
                diaAgenda = Integer.parseInt(fechaArray[0]);
                mesAgenda = Integer.parseInt(fechaArray[1]);
                anioAgenda = Integer.parseInt(fechaArray[2]);
            }
        }catch (JSONException e){
        }
        Intent calIntent = new Intent(Intent.ACTION_INSERT);
        calIntent.setType("vnd.android.cursor.item/event");
        calIntent.putExtra(CalendarContract.Events.TITLE, localagenda+" VS "+visitorAgenda);
        calIntent.putExtra(CalendarContract.Events.EVENT_LOCATION,estadioAgenda);
        calIntent.putExtra(CalendarContract.Events.DESCRIPTION, "Juego");
        GregorianCalendar calDate = new GregorianCalendar(anioAgenda,mesAgenda, diaAgenda);
        calIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
        calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                calDate.getTimeInMillis());
        calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                calDate.getTimeInMillis());
        startActivity(calIntent);

            }
        });*/


        listViewScores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                       @Override
                       public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                             TextView auxCodigo = (TextView) view.findViewById(R.id.pbp_url);
                             CharSequence ccodigo = auxCodigo.getText();
                             String codigo = ccodigo+"";
                             Intent intent = new Intent(getActivity(), PlaybyPlay_Activity.class);
                             intent.putExtra("codigo",codigo);
                             intent.putExtra("jsonData",jsonscorecontent.toString());
                             startActivity(intent);
                             }
                     });
        cargadecontenido();
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {

                        dataRefresh();
                        swipeContainer.setRefreshing(false);
                    }
                },5000);

            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_blue_light,
                android.R.color.holo_blue_dark);
        return V;
    }



    public void dataRefresh(){
        try{
            ConexionHTTP.getWebData(Configuracion.JSON_CALENDAR, this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setFecha(String dates, JSONArray jsondata) {
        this.fechas = dates;
        this.jsonscorecontent = jsondata;
        Log.i("PruebaFechas", dates);
    }

    public void cargadecontenido() {
        if(jsonscorecontent != null){
           mAdapterTable = new Adapter_scores(getActivity(), parseM.parseScores(jsonscorecontent,fechas),getContext(),jsonscorecontent);
           listViewScores.setAdapter(mAdapterTable);
            progress.setVisibility(View.GONE);
        }
    }

    @Override
    public void callback(String data) {
        if (!data.equals("") && data != null && data.indexOf("Exception") < 0) {
            try {
                jsondata = new JSONArray(data);
                cache.setVariablePermanente("Calendario", jsondata.toString());
                cargadecontenido();
            } catch (Exception ex) {
                // Analytics.logError("JuegosFragment.callback()", ex.getMessage(), ex.getCause());
            }
        }
    }
}



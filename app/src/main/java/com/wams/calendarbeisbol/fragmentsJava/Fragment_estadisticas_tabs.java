package com.wams.calendarbeisbol.fragmentsJava;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wams.calendarbeisbol.Adapters.Adapter_tabs_estadisticas;
import com.wams.calendarbeisbol.R;

/**
 * Created by WilliamO on 9/2/2016.
 */
public class Fragment_estadisticas_tabs extends Fragment {
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    boolean fragmentTransaction = true;
    Fragment fragment = null;

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_estadisticas_tabs, container, false);
            int value = getArguments().getInt("msg");

        tabLayout = (TabLayout) view.findViewById(R.id.tabs_estadisticas);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager_estadisticas);

            viewPager.setAdapter(new Adapter_tabs_estadisticas(getChildFragmentManager(),value));
            Fragment_estadisticas_tabs.tabLayout.setupWithViewPager(Fragment_estadisticas_tabs.viewPager);

        return view;
    }

    public static Fragment_estadisticas_tabs newInstance(int tipo) {
        Fragment_estadisticas_tabs fm= new Fragment_estadisticas_tabs();

        Bundle b = new Bundle();
        b.putInt("msg", tipo);
        fm.setArguments(b);
        return fm;
    }

}

package com.wams.calendarbeisbol.fragmentsJava;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.wams.calendarbeisbol.Adapters.Adapter_estadisticas_content;
import com.wams.calendarbeisbol.R;

/**
 * Created by WilliamO on 9/2/2016.
 */
public class Fragment_estadisticas_content extends Fragment {
    Adapter_estadisticas_content mAdapterTable;
    ListView listViewNoticias;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_estadisticas_content, container, false);

        listViewNoticias = (ListView) view.findViewById(R.id.listview_estadisticas_content);



        mAdapterTable = new Adapter_estadisticas_content(getActivity());
        listViewNoticias.setAdapter(mAdapterTable);

        return view;
    }



}

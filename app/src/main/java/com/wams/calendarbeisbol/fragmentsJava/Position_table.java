package com.wams.calendarbeisbol.fragmentsJava;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.wams.calendarbeisbol.Adapters.Adapter_tablePosition;
import com.wams.calendarbeisbol.Conection.ConexionHTTP;
import com.wams.calendarbeisbol.Conection.DataCallback;
import com.wams.calendarbeisbol.Libs.AlmacenamientoLocal;
import com.wams.calendarbeisbol.Libs.Configuracion;
import com.wams.calendarbeisbol.Libs.ConnectionDetector;
import com.wams.calendarbeisbol.Parse.Parse_Posiciones;
import com.wams.calendarbeisbol.R;

import org.json.JSONException;
import org.json.JSONObject;

public class Position_table extends Fragment implements DataCallback,SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout swipeContainer;
    private AlmacenamientoLocal cache;
    JSONObject jsondata;
    ConnectionDetector conexion;
    ListView listViewPosicion;
    Adapter_tablePosition mAdapterTable;
    public Parse_Posiciones parseM;
    ProgressBar progress;
    Position_table ptFragment;

    public void onStart() {
        super.onStart();
        // Analytics.iniciarSesion(getActivity());
    }
    public void onStop() {
        super.onStop();
        // Analytics.iniciarSesion(getActivity());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View V = inflater.inflate(R.layout.fragment_position, container, false);

        parseM = new Parse_Posiciones();
        conexion = new ConnectionDetector(getActivity());
        cache = new AlmacenamientoLocal(getActivity());
        listViewPosicion = (ListView) V.findViewById(R.id.listPosition);
        swipeContainer = (SwipeRefreshLayout) V.findViewById(R.id.swipeContainer);
        if (conexion.hasInternet(getActivity())) {
           // ActionBarPullToRefresh.from(getActivity()).allChildrenArePullable().listener(this).setup(mPullToRefreshLayout);
            try {
                progress = (ProgressBar) V.findViewById(R.id.progressBar1);
                progress.setVisibility(View.VISIBLE);
                ConexionHTTP.getWebData(Configuracion.JSON_POSITIONS,this);
            } catch (Exception e) {
                e.printStackTrace();
                // Analytics.logError("JSONget JuegosFragment", e.getMessage(), e.getCause());
            }
        } else {
            progress = (ProgressBar) V.findViewById(R.id.progressBar1);
            progress.setVisibility(View.VISIBLE);
            if (cache.getVariablePermanente("Posiciones") == null || cache.getVariablePermanente("Posiciones").equals("")) {
                Toast.makeText(getActivity(), getResources().getString(R.string.toast2), Toast.LENGTH_LONG).show();
            } else {
                try {
                    jsondata = new JSONObject(cache.getVariablePermanente("Posiciones"));
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                cargadecontenido();
           }
        }

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {

                        dataRefresh();
                        swipeContainer.setRefreshing(false);
                    }
                }, 5000);

            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_blue_light,
                android.R.color.holo_blue_dark);

        //return inflater.inflate(R.layout.fragment_position, container, false);
        return V;
    }

    public void dataRefresh(){
       try{
           ConexionHTTP.getWebData(Configuracion.JSON_POSITIONS,this);
       }catch (Exception e){
           e.printStackTrace();
       }
    }

    public void callback(String data) {
        if (!data.equals("") && data != null && data.indexOf("Exception") < 0) {
            try {
                jsondata = new JSONObject(data);
                cache.setVariablePermanente("Posiciones", jsondata.toString());
                cargadecontenido();
            } catch (Exception ex) {
                // Analytics.logError("JuegosFragment.callback()", ex.getMessage(), ex.getCause());
            }
        }
    }

    public void cargadecontenido() {
            String text = getResources().getString(R.string.toast2);
            if (jsondata == null) {
                Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
            }

            mAdapterTable = new Adapter_tablePosition(getActivity(), parseM.parseData(jsondata));
            progress.setVisibility(View.GONE);
            listViewPosicion.setAdapter(mAdapterTable);
            listViewPosicion.setOnItemClickListener(new onClickList());
        }

    @Override
    public void onRefresh() {

    }

    class onClickList implements AdapterView.OnItemClickListener {
        onClickList() {
        }
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
        }
    }
}




package com.wams.calendarbeisbol.DecoratosCalendar;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.wams.calendarbeisbol.R;
import com.wams.calendarbeisbol.libsCalendar.CalendarDay;
import com.wams.calendarbeisbol.libsCalendar.DayViewDecorator;
import com.wams.calendarbeisbol.libsCalendar.DayViewFacade;


/**
 * Use a custom selector
 */
public class MySelectorDecorator implements DayViewDecorator {

    private final Drawable drawable;

    public MySelectorDecorator(Activity context) {
        drawable = context.getResources().getDrawable(R.drawable.my_selector);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return true;
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setSelectionDrawable(drawable);
    }
}

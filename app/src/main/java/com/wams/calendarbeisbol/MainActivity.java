package com.wams.calendarbeisbol;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.wams.calendarbeisbol.Adapters.Adapter_NavigationDrawer_items;
import com.wams.calendarbeisbol.fragmentsJava.Fragment_Scores;
import com.wams.calendarbeisbol.fragmentsJava.Fragment_estadisticas;
import com.wams.calendarbeisbol.fragmentsJava.Fragment_noticias;
import com.wams.calendarbeisbol.fragmentsJava.Position_table;

public class MainActivity extends AppCompatActivity {

    String token;
    boolean thread_running = true;
    Toolbar tb;
    TextView title;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private Adapter_NavigationDrawer_items drawer_adapter;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    Toolbar toolbar;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private String[] mNavigationDrawerItemTitles;
    String app_server_url = "http://wamsmobile.com/phpServer_calendario/fcm_insert.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FirebaseMessaging.getInstance().subscribeToTopic("test");
        String a = FirebaseInstanceId.getInstance().getToken();

       // FirebaseMessaging.getInstance().subscribeToTopic("test");
        //FirebaseInstanceId.getInstance().getToken();

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(thread_running){
                    token = FirebaseInstanceId.getInstance().getToken();
                    if(token!= null){
                        Log.i(" Device token is: ",token);
                        FirebaseIDService fm = new FirebaseIDService();
                        fm.registerToken(token);
                        thread_running = false;
                    }else{
                        System.out.println(" Token not loaded");
                    }
                    try{
                        Thread.sleep(1000);
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();

        mTitle = mDrawerTitle = getTitle();
        mNavigationDrawerItemTitles= getResources().getStringArray(R.array.opciones_menu);


        boolean fragmentTransaction = true;
        Fragment fragment = null;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView)toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        title.setText("RESULTADOS");

        mDrawerList = (ListView)findViewById(R.id.left_drawer);
        drawer_adapter = new Adapter_NavigationDrawer_items(this);
        mDrawerList.setAdapter(drawer_adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        setupToolbar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        setupDrawerToggle();


        /* ****************TABLAYOUT AND VIEWPAGER****************** */
        fragment = new Fragment_Scores();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame,fragment)
                .commit();
    }
    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 1:
                fragment = new Fragment_Scores();
                break;
            case 2:
               /* SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE );
                final String token = sharedPreferences.getString(getString(R.string.FCM_TOKEN),"");
                final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                Log.i("TOKEN : ",  refreshedToken);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, app_server_url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                            }
                        },new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("fcm_token",refreshedToken);
                        return params;
                    }
                };
                MySingleton.getmInstance(MainActivity.this).addToRequest(stringRequest);
*/
                fragment = new Position_table();
                break;
            case 3:
                fragment = new Fragment_estadisticas();
                break;
            case 4:
                fragment = new Fragment_noticias();

                break;

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(mNavigationDrawerItemTitles[position-1]);
            mDrawerLayout.closeDrawer(mDrawerList);
        }else{
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(CharSequence title) {
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mTitle = title;
        TextView titulo = (TextView)toolbar.findViewById(R.id.toolbar_title);
        titulo.setText(mTitle);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    void setupToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    void setupDrawerToggle(){
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this,mDrawerLayout,toolbar,R.string.app_name, R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        mDrawerToggle.syncState();
    }

}


package com.wams.calendarbeisbol;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wams.calendarbeisbol.Adapters.Adapter_pbp_Hechos;
import com.wams.calendarbeisbol.Adapters.Adapter_pbp_Scores;
import com.wams.calendarbeisbol.Conection.ConexionHTTP;
import com.wams.calendarbeisbol.Conection.DataCallback;
import com.wams.calendarbeisbol.Libs.AlmacenamientoLocal;
import com.wams.calendarbeisbol.Libs.ConnectionDetector;
import com.wams.calendarbeisbol.Loader_img.ImageLoader;
import com.wams.calendarbeisbol.Parse.Parse_pbp;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by WilliamO on 7/21/2016.
 */
public class PlaybyPlay_Activity extends AppCompatActivity implements DataCallback {

    ArrayList<HashMap<String, String>> scoresParsePbp;
    ArrayList<HashMap<String, String>> hechosParsePbp;
    private AlmacenamientoLocal cache;
    Adapter_pbp_Hechos mAdapterTableHechos;
    Adapter_pbp_Scores mAdapterTableScores;
    public Parse_pbp parseM;
    ConnectionDetector conexion;
    String urlPbp="";
    JSONObject jsonDatospbp;
    JSONObject jsonScorepbp;
    JSONObject jsonHechopbp;
    ListView listViewHechos;
    RecyclerView recyclerViewScores;
    public ImageLoader imageLoader;
    ProgressBar progress;
    private SwipeRefreshLayout swipeContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playbybplay);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarPBP);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        imageLoader = new ImageLoader(this);
        imageLoader.setImageDefault(R.drawable.placeholder);
        swipeContainer      = (SwipeRefreshLayout)findViewById(R.id.swipeContainerPbp);
        listViewHechos      = (ListView)findViewById(R.id.listviewPlaybyPlay);
        recyclerViewScores  = (RecyclerView) findViewById(R.id.recyclerViewInfo);
        conexion = new ConnectionDetector(this);
        cache = new AlmacenamientoLocal(this);
        parseM = new Parse_pbp();
        Bundle b = this.getIntent().getExtras();
        if(b!=null){
            urlPbp= b.getString("codigo");
        }
        if (conexion.hasInternet(this)) {
            try {
                progress = (ProgressBar) findViewById(R.id.progressBar2);
                progress.setVisibility(View.VISIBLE);
                ConexionHTTP.getWebData(urlPbp,this);
            } catch (Exception e) {
                e.printStackTrace();
               }
        }else{
            TextView msj =(TextView)findViewById(R.id.mensaje_sinconexion);
            msj.setVisibility(View.VISIBLE);
            msj.setText("SIN CONEXION A INTERNET");

        }

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {

                        hechosParsePbp.clear();
                        scoresParsePbp.clear();
                        dataRefresh();
                        swipeContainer.setRefreshing(false);
                        mAdapterTableHechos.notifyDataSetChanged();
                    }
                }, 5000);

            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_blue_light,
                android.R.color.holo_blue_dark);

    }

    public void dataRefresh(){
        try{
            ConexionHTTP.getWebData(urlPbp, this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void elementosLocales(String data){
        TextView nombreLocal  = (TextView)findViewById(R.id.nameHome);
        TextView nombreVis = (TextView)findViewById(R.id.nameVispbp);
        TextView sNombreLocal = (TextView)findViewById(R.id.infoHomename);
        TextView sNombreVis = (TextView)findViewById(R.id.infoVisname);
        TextView cHomePbp = (TextView)findViewById(R.id.carrerasHomepbp);
        TextView cVisPbp = (TextView)findViewById(R.id.carrerasVispbp);
        TextView hitsHomePbp = (TextView)findViewById(R.id.infoHitsHome);
        TextView hitsVisPbp = (TextView)findViewById(R.id.infoHitsVis);
        TextView errorHomePbp = (TextView)findViewById(R.id.infoErroresHome);
        TextView errorVisPbp = (TextView)findViewById(R.id.infoErroresVis);

        TextView carrerasAll = (TextView) findViewById(R.id.infoCarrera);
        TextView hitsAll = (TextView) findViewById(R.id.infoHits);
        TextView erroresAll = (TextView) findViewById(R.id.infoErroes);

        TextView carrerasHomPbp = (TextView)findViewById(R.id.infoCarreraHome);
        TextView carrerasVisPbp = (TextView)findViewById(R.id.infoCarreraVis);

        ImageView logoHome = (ImageView)findViewById(R.id.img_home_pbp);
        ImageView logoVis = (ImageView)findViewById(R.id.img_vis_pbp);

        HashMap<String, String> map = new HashMap();
        try {
            JSONObject d = jsonDatospbp.getJSONObject("juego");
            JSONObject f = jsonDatospbp.getJSONObject("visitante");
            JSONObject g = jsonDatospbp.getJSONObject("homeclub");

            String urlimageHome = g.getString(Parse_pbp.TAG_IMG_LOCAL);
            String urlimageVis = f.getString(Parse_pbp.TAG_IMG_VIS);


            String nombreHome = g.getString(Parse_pbp.TAG_NAME_LOCAL);
            String snombreHome = g.getString(Parse_pbp.TAG_SNAME_LOCAL);

            String nombreVisitante = f.getString(Parse_pbp.TAG_NAME_VIS);
            String snombreVisitante = f.getString(Parse_pbp.TAG_SNAME_VIS);

            String carreras = d.getString(Parse_pbp.TAG_C);
            String hits = d.getString(Parse_pbp.TAG_H);
            String error = d.getString(Parse_pbp.TAG_E);

            String[] auxCarreras = carreras.split(",");
            String[] auxErrores = error.split(",");
            String[] auxHits = hits.split(",");

            nombreLocal.setText(nombreHome);
            sNombreLocal.setText(snombreHome);

            nombreVis.setText(nombreVisitante);
            sNombreVis.setText(snombreVisitante);

            cHomePbp.setText(auxCarreras[0]);
            cVisPbp.setText(auxCarreras[1]);

            carrerasHomPbp.setText(auxCarreras[0]);
            carrerasVisPbp.setText(auxCarreras[1]);

            hitsHomePbp.setText(auxHits[0]);
            hitsVisPbp.setText(auxHits[1]);

            errorHomePbp.setText(auxErrores[0]);
            errorVisPbp.setText(auxErrores[1]);




            Glide.with(this)
                    .load(urlimageHome)
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .into(logoHome);

            Glide.with(this)
                    .load(urlimageVis)
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .into(logoVis);

            carrerasAll.setText("C");
            hitsAll.setText("H");
            erroresAll.setText("E");
        }catch (JSONException e){e.printStackTrace();}

    }
    @Override
    public void callback(String data) {
        if (!data.equals("") && data != null && data.indexOf("Exception") < 0) {
            try {
                jsonDatospbp= new JSONObject(data);
                jsonScorepbp= new JSONObject(data);
                jsonHechopbp = new JSONObject(data);
                progress.setVisibility(View.GONE);
                elementosLocales(data);
                cargadecontenido();
                 //cache.setVariablePermanente("Calendario", jsondata.toString());
                // cargadecontenido();
            } catch (Exception ex) {
                // Analytics.logError("JuegosFragment.callback()", ex.getMessage(), ex.getCause());
            }
        }
    }
    public void cargadecontenido() {

            scoresParsePbp = parseM.parseScoresPbp(jsonScorepbp);
            hechosParsePbp = parseM.parseHechosPbp(jsonHechopbp);
            mAdapterTableHechos = new Adapter_pbp_Hechos(this,hechosParsePbp);
            mAdapterTableScores = new Adapter_pbp_Scores(this,scoresParsePbp);

            //recyclerViewScores.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.HORIZONTAL));
            recyclerViewScores.setLayoutManager(new LinearLayoutManager(PlaybyPlay_Activity.this, LinearLayoutManager.HORIZONTAL, false));
            recyclerViewScores.setAdapter(mAdapterTableScores);
            recyclerViewScores.scrollToPosition(new Adapter_pbp_Scores(this,scoresParsePbp).getItemCount() - 1);
            listViewHechos.setAdapter(mAdapterTableHechos);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

package com.wams.calendarbeisbol.libsCalendar;

import android.support.annotation.NonNull;
import android.support.v4.util.SparseArrayCompat;

/**
 * Pager adapter backing the calendar view
 */
class MonthPagerAdapter extends CalendarPagerAdapter<MonthView> {

    MonthPagerAdapter(MaterialCalendarView mcv) {
        super(mcv);
    }

    @Override
    protected MonthView createView(int position) {
        return new MonthView(mcv, getItem(position), mcv.getFirstDayOfWeek());
    }

    @Override
    protected int indexOf(MonthView view) {
        com.wams.calendarbeisbol.libsCalendar.CalendarDay month = view.getMonth();
        return getRangeIndex().indexOf(month);
    }

    @Override
    protected boolean isInstanceOfView(Object object) {
        return object instanceof MonthView;
    }

    @Override
    protected DateRangeIndex createRangeIndex(com.wams.calendarbeisbol.libsCalendar.CalendarDay min, com.wams.calendarbeisbol.libsCalendar.CalendarDay max) {
        return new Monthly(min, max);
    }

    public static class Monthly implements DateRangeIndex {

        private final com.wams.calendarbeisbol.libsCalendar.CalendarDay min;
        private final int count;

        private SparseArrayCompat<com.wams.calendarbeisbol.libsCalendar.CalendarDay> dayCache = new SparseArrayCompat<>();

        public Monthly(@NonNull com.wams.calendarbeisbol.libsCalendar.CalendarDay min, @NonNull com.wams.calendarbeisbol.libsCalendar.CalendarDay max) {
            this.min = com.wams.calendarbeisbol.libsCalendar.CalendarDay.from(min.getYear(), min.getMonth(), 1);
            max = com.wams.calendarbeisbol.libsCalendar.CalendarDay.from(max.getYear(), max.getMonth(), 1);
            this.count = indexOf(max) + 1;
        }

        public int getCount() {
            return count;
        }

        public int indexOf(com.wams.calendarbeisbol.libsCalendar.CalendarDay day) {
            int yDiff = day.getYear() - min.getYear();
            int mDiff = day.getMonth() - min.getMonth();

            return (yDiff * 12) + mDiff;
        }

        public com.wams.calendarbeisbol.libsCalendar.CalendarDay getItem(int position) {

            com.wams.calendarbeisbol.libsCalendar.CalendarDay re = dayCache.get(position);
            if (re != null) {
                return re;
            }

            int numY = position / 12;
            int numM = position % 12;

            int year = min.getYear() + numY;
            int month = min.getMonth() + numM;
            if (month >= 12) {
                year += 1;
                month -= 12;
            }

            re = CalendarDay.from(year, month, 1);
            dayCache.put(position, re);
            return re;
        }
    }
}

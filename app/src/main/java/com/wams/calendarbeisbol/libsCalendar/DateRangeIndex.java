package com.wams.calendarbeisbol.libsCalendar;

/**
 * Use math to calculate first days of months by postion from a minium date
 */
interface DateRangeIndex {

    int getCount();

    int indexOf(com.wams.calendarbeisbol.libsCalendar.CalendarDay day);

    CalendarDay getItem(int position);
}

package com.wams.calendarbeisbol.Parse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Parse_Posiciones {
    public static final String TAG_DIF = "dif";
    public static final String TAG_ID = "id";
    public static final String TAG_IMG = "img";
    public static final String TAG_JG = "jg";
    public static final String TAG_JJ = "jj";
    public static final String TAG_JP = "jp";
    public static final String TAG_JSONDATA = "jsondata";
    public static final String TAG_NAME = "name";
    public static final String TAG_POSITION = "position";

    public ArrayList<HashMap<String, String>> parseData(JSONObject jsondata) {
        ArrayList<HashMap<String, String>> contactList = new ArrayList();
        try {
            JSONArray equiposjson = jsondata.getJSONArray("Equipos");
            for (int k = 0; k < equiposjson.length(); k++) {
                HashMap<String, String> map = new HashMap();
                JSONObject d = equiposjson.getJSONObject(k);
                String position = d.getString(TAG_POSITION);
                String imagen = d.getString(TAG_IMG);
                String nombre = d.getString(TAG_NAME);
                String juegosj = d.getString(TAG_JJ);
                String juegosg = d.getString(TAG_JG);
                String juegosp = d.getString(TAG_JP);
                String idfavorito = d.getString(TAG_ID);
                String diferencia = d.getString(TAG_DIF);
                map.put(TAG_POSITION, position);
                map.put(TAG_IMG, imagen);
                map.put(TAG_NAME, nombre);
                map.put(TAG_JJ, juegosj);
                map.put(TAG_JG, juegosg);
                map.put(TAG_JP, juegosp);
                map.put(TAG_DIF, diferencia);
                contactList.add(map);
                contactList.contains("asd");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return contactList;
    }
}

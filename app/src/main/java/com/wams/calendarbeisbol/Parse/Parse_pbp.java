package com.wams.calendarbeisbol.Parse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by WilliamO on 7/21/2016.
 */
public class Parse_pbp {

    public static final String TAG_IMG_LOCAL = "img";
    public static final String TAG_NAME_LOCAL = "name";
    public static final String TAG_SNAME_LOCAL = "sname";
    public static final String TAG_IMG_VIS = "img";
    public static final String TAG_NAME_VIS = "name";
    public static final String TAG_SNAME_VIS = "sname";
    public static final String TAG_PARTE = "parte";
    public static final String TAG_ACCION = "accion";

    public static final String TAG_INNING_SCORE = "inning";
    public static final String TAG_INNING_HECHOS = "in";
    public static final String TAG_VISIN= "visitor";
    public static final String TAG_LOCALIN= "home";




    public static final String TAG_C  = "c";
    public static final String TAG_H  = "h";
    public static final String TAG_E  = "e";
    public static final String TAG_C1 = "p";
    public static final String TAG_H1 = "t";
    public static final String TAG_E1 = "d";

    ArrayList<HashMap<String, String>> pbpScoresList  = new ArrayList();
    ArrayList<HashMap<String, String>> pbpHechosList  = new ArrayList();

    public ArrayList<HashMap<String, String>> parseScoresPbp(JSONObject jsonScoredata){
        HashMap<String, String> map;
        try {
            JSONArray score = jsonScoredata.getJSONArray("score");
            if(!score.equals("")){
                for (int k = 0; k < score.length(); k++) {
                    JSONObject f = score.getJSONObject(k);
                    map = new HashMap();
                    String inning = f.getString(TAG_INNING_SCORE);
                    String homeIN = f.getString(TAG_LOCALIN);
                    String visIN = f.getString(TAG_VISIN);
                    map.put(TAG_INNING_SCORE,inning);
                    map.put(TAG_LOCALIN,homeIN);
                    map.put(TAG_VISIN,visIN);
                    pbpScoresList.add(map);
                }
            }
        }catch (JSONException e){e.printStackTrace();}
        return pbpScoresList;
    }

    public ArrayList<HashMap<String, String>> parseHechosPbp(JSONObject jsondata){

        try {
            JSONArray hechos = jsondata.getJSONArray("hechos");
            HashMap<String, String> map;
            for (int k = 0; k < hechos.length(); k++) {
                JSONObject  d = hechos.getJSONObject(k);
                map = new HashMap();
                String inning = d.getString(TAG_INNING_HECHOS);
                String parte = d.getString(TAG_PARTE);
                String accion = d.getString(TAG_ACCION);
                map.put(TAG_INNING_HECHOS, inning);
                map.put(TAG_PARTE, parte);
                map.put(TAG_ACCION, accion);
                pbpHechosList.add(map);
            }
        }catch (JSONException e){e.printStackTrace();}

        return pbpHechosList;
    }
}

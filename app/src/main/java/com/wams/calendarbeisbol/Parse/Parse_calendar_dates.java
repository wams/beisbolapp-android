package com.wams.calendarbeisbol.Parse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by WilliamO on 6/17/2016.
 */
public class Parse_calendar_dates {
    public static final String TAG_FECHA        = "fecha";
    public static final String TAG_ESTADIO      = "estadio";
    public static final String TAG_IDLOCAL      = "id_home";
    public static final String TAG_LOCAL        = "home";
    public static final String TAG_LOCAL_IMG    = "homeimg";
    public static final String TAG_IDVISITANTE  = "idvisitor";
    public static final String TAG_VISITANTE    = "visitor";
    public static final String TAG_VISITANTE_IMG= "visitorimg";
    public static final String TAG_C            = "c";
    public static final String TAG_H            = "h";
    public static final String TAG_E            = "e";
    public static final String TAG_C1           = "p";
    public static final String TAG_H1           = "t";
    public static final String TAG_E1           = "d";
    public static final String TAG_HORA         = "hora";
    public static final String TAG_INNING       = "inning";
    public static final String TAG_MINUTOAMINUTO = "minutoaminuto";
    public static final String TAG_IDJUEGOS = "codigo";
    ArrayList<String> contactList;
    ArrayList<HashMap<String, String>> scoresList;


    public ArrayList<HashMap<String, String>> parseScores(JSONArray jsondata,String fecha) {

        scoresList = new ArrayList();
        try {
            for (int i = 0; i <=jsondata.length(); i++) {
                HashMap<String, String> map = new HashMap();
                JSONObject d = jsondata.getJSONObject(i);
                String fechas = d.getString(TAG_FECHA);

                if(fecha.equals(fechas)){
                    String estadio = d.getString(TAG_ESTADIO);
                    String id_local = d.getString(TAG_IDLOCAL);
                    String local = d.getString(TAG_LOCAL);
                    String imglocal = d.getString(TAG_LOCAL_IMG);
                    String id_visitor = d.getString(TAG_IDVISITANTE);
                    String visitor = d.getString(TAG_VISITANTE);
                    String imgvisitante = d.getString(TAG_VISITANTE_IMG);
                    String hora = d.getString(TAG_HORA);
                    String innings = d.getString(TAG_INNING);
                    String playByPlay = d.getString(TAG_MINUTOAMINUTO);
                    String codigo = d.getString(TAG_IDJUEGOS);

                    String auxCarreras = d.getString(TAG_C);
                    String auxHits = d.getString(TAG_H);
                    String auxErrores  = d.getString(TAG_E);

                    String[]  carrerasArray = auxCarreras.split(",");
                    String[]  hitsArray = auxHits.split(",");
                    String[]  erroresArray = auxErrores.split(",");

                    map.put(TAG_FECHA,fechas);
                    map.put(TAG_ESTADIO,estadio);
                    map.put(TAG_IDLOCAL,id_local);
                    map.put(TAG_LOCAL,local);
                    map.put(TAG_LOCAL_IMG,imglocal);
                    map.put(TAG_IDVISITANTE,id_visitor);
                    map.put(TAG_VISITANTE,visitor);
                    map.put(TAG_VISITANTE_IMG,imgvisitante);

                    map.put(TAG_C,carrerasArray[0]);
                    map.put(TAG_C1,carrerasArray[1]);
                    map.put(TAG_H,hitsArray[0]);
                    map.put(TAG_H1,hitsArray[1]);
                    map.put(TAG_E,erroresArray[0]);
                    map.put(TAG_E1,erroresArray[1]);

                    map.put(TAG_HORA,hora);
                    map.put(TAG_INNING,innings);
                    map.put(TAG_MINUTOAMINUTO,playByPlay);
                    map.put(TAG_IDJUEGOS,codigo);
                    scoresList.add(map);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return scoresList;
    }

    public ArrayList<String> parseDates (JSONArray jsondata/*, String idequipo, int calendars*/){
        contactList = new ArrayList<>();

        for(int i=0; i<=jsondata.length();i++)
        {
            try
            {
                if(i==0){
                    JSONObject c = jsondata.getJSONObject(i);
                    String fecha = c.getString(TAG_FECHA);
                    contactList.add(fecha);
                }
                else{
                    JSONObject c = jsondata.getJSONObject(i);
                    String fecha = c.getString(TAG_FECHA);

                    if(!contactList.contains(fecha)){
                        contactList.add(fecha);
                    }
                }
            } catch (JSONException e){e.printStackTrace();}
        }

        return contactList;
    }
}



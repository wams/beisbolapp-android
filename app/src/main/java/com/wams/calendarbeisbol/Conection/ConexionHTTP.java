package com.wams.calendarbeisbol.Conection;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class ConexionHTTP {

	public static Handler UIHandler;

	static {
		UIHandler = new Handler(Looper.getMainLooper());
	}

	private static void runOnUI(Runnable runnable) {
		UIHandler.post(runnable);
	}

	/**
	 * Devuelve los datos de una conexion de forma async dentro de un metodo callback
	 * @param url
	 * @param callback 
	 * @throws IOException
	 */
	public static void getWebData(final String url, final DataCallback callback) throws IOException
	{
		Thread t = new Thread(new Runnable()
		{
			public void run()
			{
				try
				{

					HttpClient httpclient = new DefaultHttpClient();
					//String encodedurl = URLEncoder.encode(url, "UTF-8");
					HttpPost httppost = new HttpPost(url);
					String urls = url;

					HttpResponse response = httpclient.execute(httppost);
					final String result = inputStreamToString(response.getEntity().getContent()).toString();


					ConexionHTTP.runOnUI(new Runnable() {
						public void run() {
							callback.callback(result);
						}
					});					

				}
				catch (final Exception ex)
				{
					ConexionHTTP.runOnUI(new Runnable()
					{
						public void run()
						{
							callback.callback("Exception (" + ex.getClass() + "): " + ex.getMessage());
							//Analytics.logError("ConexionHTTP.getWebData().run()", ex.getMessage(), ex.getCause());
						}
					});
				}
			}
		});
		t.start();
	}

	public static void getDataHttpOk(final String url, final DataCallback callback) throws IOException
	{
		
	}
	
	
	public static void updatebd(final String token, final String option) throws IOException
	{
		Thread t = new Thread(new Runnable()
		{
			public void run()
			{

				OkHttpClient client = new OkHttpClient();
				RequestBody body = new FormBody.Builder()
						.add("option",option)
						.add("token",token)
						.build();
				Request request = new Request.Builder()
						.url("http://wamsmobile.com/phpServer_calendario/update_option.php")
						.post(body)
						.build();
				try {
					client.newCall(request).execute();
					Log.i("token",token);
				} catch (IOException e) {
					e.printStackTrace();
				}

				/*try
				{
					HttpClient httpclient = new DefaultHttpClient();
					HttpGet httppost = new HttpGet(url);

					HttpResponse response = httpclient.execute(httppost);
					final String result = inputStreamToString(response.getEntity().getContent()).toString();


					ConexionHTTP.runOnUI(new Runnable() {
						public void run() {
							callback.callback(result);
						}
					});					

				}
				catch (final Exception ex)
				{
					ConexionHTTP.runOnUI(new Runnable()
					{
						public void run()
						{
							callback.callback("Exception (" + ex.getClass() + "): " + ex.getMessage());
							//Analytics.logError("ConexionHTTP.getWebData().run()", ex.getMessage(), ex.getCause());
						}
					});
				}*/
			}
		});
		t.start();
	}

	/**
	 * Devuelve los datos de una conexi�n forma sincrona
	 * @param url
	 * @return String
	 * @throws IOException
	 */
	public static String getWebDataSync(final String url) throws IOException
	{
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);

			HttpResponse response = httpclient.execute(httppost);
			final String result = inputStreamToString(response.getEntity().getContent()).toString();

			return result;
		}
		catch (final Exception ex)
		{
			//Analytics.logError("ConexionHTTP.getWebDataSync()", ex.getMessage(), ex.getCause());
			return "Exception (" + ex.getClass() + "): " + ex.getMessage();
		}
	}	
	

	private static String inputStreamToString(final InputStream stream) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(stream, "ISO-8859-1"));
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = br.readLine()) != null) {
			sb.append(line + "\n");
		}
		br.close();
		return sb.toString();
	}	

}

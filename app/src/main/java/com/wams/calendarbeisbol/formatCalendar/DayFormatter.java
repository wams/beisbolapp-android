package com.wams.calendarbeisbol.formatCalendar;

import android.support.annotation.NonNull;

import com.wams.calendarbeisbol.libsCalendar.CalendarDay;

import java.text.SimpleDateFormat;

/**
 * Supply labels for a given day. Default implementation is to format using a {@linkplain SimpleDateFormat}
 */
public interface DayFormatter {

    /**
     * Format a given day into a string
     *
     * @param day the day
     * @return a label for the day
     */
    @NonNull
    String format(@NonNull CalendarDay day);

    /**
     * Default implementation used by {@linkplain com.wams.calendarbeisbol.libsCalendar.MaterialCalendarView}
     */
    public static final com.wams.calendarbeisbol.formatCalendar.DayFormatter DEFAULT = new DateFormatDayFormatter();
}

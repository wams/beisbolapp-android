package com.wams.calendarbeisbol;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by WilliamO on 8/24/2016.
 */
public class FirebaseIDService extends FirebaseInstanceIdService {

    private static final String TAG = "FirebaseIDService";

    /*@Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        System.out.println("Token; "+refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */


    /*private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
    }*/

   @Override
    public void onTokenRefresh() {
    String token = FirebaseInstanceId.getInstance().getToken();
    registerToken(token);
    }
   public void registerToken(String token) {
    OkHttpClient client = new OkHttpClient();
    RequestBody body = new FormBody.Builder()
                .add("Token",token)
                .build();
        Request request = new Request.Builder()
            .url("http://wamsmobile.com/phpServer_calendario/register.php")
            .post(body)
            .build();
        try {
            client.newCall(request).execute();
            } catch (IOException e) {
            e.printStackTrace();
            }
    }
}

package com.wams.calendarbeisbol.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.wams.calendarbeisbol.fragmentsJava.Fragment_estadisticas_content;

/**
 * Created by WilliamO on 9/2/2016.
 */
public class Adapter_tabs_estadisticas extends FragmentStatePagerAdapter {

    String[] pitcheoArray = {"Efectividad","Ponches","Ganados","Innings","Salvados","Mantenidos"};
    String[] bateoArray = {"Average","Home Runs","Impulsadas","Anotadas","Slugging","Robadas"};
    int tipo;
    public Adapter_tabs_estadisticas(FragmentManager fm, int value) {
        super(fm);
        tipo = value;
    }

    @Override
    public Fragment getItem(int position) {

        if(tipo == 1){
            Fragment_estadisticas_content fm = new Fragment_estadisticas_content();
            return fm;
        }
        if(tipo == 2){
            Fragment_estadisticas_content fm = new Fragment_estadisticas_content();
            return fm;
        }
        Fragment_estadisticas_content fm = new Fragment_estadisticas_content();
        return fm;
    }

    @Override
    public int getCount() {
        return pitcheoArray.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(tipo == 1){
            return pitcheoArray[position];
        }else if (tipo == 2){
            return  bateoArray[position];
        }
        return pitcheoArray[position];
    }
}

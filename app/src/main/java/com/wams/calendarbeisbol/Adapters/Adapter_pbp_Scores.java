package com.wams.calendarbeisbol.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wams.calendarbeisbol.Parse.Parse_pbp;
import com.wams.calendarbeisbol.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by WilliamO on 7/26/2016.
 */
public class Adapter_pbp_Scores extends RecyclerView.Adapter<Adapter_pbp_Scores.MyViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, String>> scoresPbp;
    int i = 0;

    public Adapter_pbp_Scores(Activity a, ArrayList<HashMap<String,String>> scoresPbp){
        this.activity = a;
        this.scoresPbp = scoresPbp;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recyclerview_scores_pbp, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        HashMap<String, String> juego = new HashMap<String, String>();
        int hola = scoresPbp.size();

        i += 1;
        if ( hola == 0) {
            holder.in.setText(position + 1+ "");
            holder.scoresHome.setText(" ");
            holder.scoresVis.setText(" ");
        }else if (position + 1 > scoresPbp.size()) {
            holder.in.setText(position + 1+"");
            holder.scoresHome.setText(" ");
            holder.scoresVis.setText(" ");
        }else{
            juego = scoresPbp.get(position);
            holder.in.setText(juego.get(Parse_pbp.TAG_INNING_SCORE));
            holder.scoresHome.setText(juego.get(Parse_pbp.TAG_LOCALIN));
            holder.scoresVis.setText(juego.get(Parse_pbp.TAG_VISIN));
        }
    }

    @Override
    public int getItemCount() {
        if (scoresPbp.size() > 9){
            return scoresPbp.size();
        }else {
            return 9;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView in,scoresHome,scoresVis;
        public MyViewHolder(View view) {
            super(view);
            in = (TextView) view.findViewById(R.id.inning_scores_pbp);
            scoresHome = (TextView) view.findViewById(R.id.homeScore_scores_pbp);
            scoresVis  = (TextView) view.findViewById(R.id.visScore_scores_pbp);
        }
    }

}


package com.wams.calendarbeisbol.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.wams.calendarbeisbol.Conection.ConexionHTTP;
import com.wams.calendarbeisbol.R;

import java.io.IOException;

/**
 * Created by WilliamO on 10/10/2016.
 */
public class Adapter_NavigationDrawer_items extends BaseAdapter {
    String[] opciones;
    Context context;
    private static LayoutInflater inflater = null;
    int[] icons = {R.drawable.resultados,R.drawable.posiciones,R.drawable.estadisticas,R.drawable.noticias,R.drawable.alertas};
        public Adapter_NavigationDrawer_items(Context context){
         opciones = context.getResources().getStringArray(R.array.opciones_menu);
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

    @Override
    public int getCount() {
        return opciones.length + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final String token = FirebaseInstanceId.getInstance().getToken();
        View vi = convertView;
        if(position == 0){
            vi = inflater.inflate(R.layout.nav_header_main,null);
        }else{
            vi = inflater.inflate(R.layout.custom_row_drawer,null);
            if(position == 5) {
                TextView opcion = (TextView)vi.findViewById(R.id.txt_drawer_opcion);
                ImageView icon = (ImageView)vi.findViewById(R.id.image_drawer_icon);
                final Switch sw = (Switch)vi.findViewById(R.id.switch_drawer);
                opcion.setText(opciones[position-1]);
                icon.setImageResource(icons[position-1]);
                sw.setVisibility(View.VISIBLE);
                sw.setChecked(true);
                sw.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(sw.isChecked()){
                            try {
                                ConexionHTTP.updatebd(token,"1");
                            }catch (IOException e){
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                ConexionHTTP.updatebd(token,"0");
                            }catch (IOException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }else{
                TextView opcion = (TextView)vi.findViewById(R.id.txt_drawer_opcion);
                ImageView icon = (ImageView)vi.findViewById(R.id.image_drawer_icon);
                opcion.setText(opciones[position-1]);
                icon.setImageResource(icons[position-1]);
            }
        }
        return vi;
    }
}

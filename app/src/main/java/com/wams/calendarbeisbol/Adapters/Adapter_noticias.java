package com.wams.calendarbeisbol.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wams.calendarbeisbol.R;

/**
 * Created by WilliamO on 9/1/2016.
 */
public class Adapter_noticias extends BaseAdapter {
    private Activity activity;
    String[] imagenesArray = {"http://az838082.vo.msecnd.net/fotos_meridiano/14726838951657748470.jpg","http://www.liderendeportes.com/getattachment/a5b1c321-0bf5-4917-bb4c-c93818b845b7","http://az838082.vo.msecnd.net/fotos_meridiano/147282452679358538.jpg","http://az838082.vo.msecnd.net/fotos_meridiano/1472664395239101957.jpg"};
    String[] noticiaArray  ={"Altuve sonó triple en el triunfo de los Astros","Pitcheo de Colorado no dio tregua a los Dodgers","Intensidad y poder, llevan a Rougned Odor al estrellato","Magallanes comenzará entrenamientos el 12 de septiembre"};
    private static LayoutInflater inflater = null;
    Context context;

    public Adapter_noticias(Activity a,Context c){
        context = c;
        activity = a;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return noticiaArray.length;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.items_listview_noticias, parent, false);
        }

        vi = inflater.inflate(R.layout.items_listview_noticias,null);


        TextView nombre = (TextView) vi.findViewById(R.id.lbl_noticias_mensaje);

        ImageView imagen = (ImageView) vi.findViewById(R.id.img_noticias);

        ImageView image_share = (ImageView) vi.findViewById(R.id.imageView_share);

        String urlImagen = imagenesArray[position];

        // Setting all values in listview
        nombre.setText(noticiaArray[position]);
        Glide.with(activity)
                .load(urlImagen)
                .placeholder(R.drawable.placeholder)
                .into(imagen);


        image_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Here is the share content body";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });




        return vi;


    }
}

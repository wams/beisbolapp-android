package com.wams.calendarbeisbol.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wams.calendarbeisbol.Loader_img.ImageLoader;
import com.wams.calendarbeisbol.Parse.Parse_calendar_dates;
import com.wams.calendarbeisbol.R;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;

/**
 * Created by WilliamO on 7/6/2016.
 */
public class Adapter_scores extends BaseAdapter{
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater = null;
    public ImageLoader imageLoader;
    Button botonAgendar;
    private Context context;
    int diaAgenda,mesAgenda,anioAgenda;
    JSONArray jsonscorecontent;


    public Adapter_scores(Activity a, ArrayList<HashMap<String,String>> d, Context context, JSONArray jsonscorecontent){
        activity = a;
        data = d;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(activity.getApplicationContext());
        imageLoader.setImageDefault(R.drawable.placeholder);
        imageLoader.setImageSize(100);
        this.context = context;
        this.jsonscorecontent = jsonscorecontent;
    }

    @Override
    public int getCount() {return data.size();}

    @Override
    public Object getItem(int position) {return position;}

    @Override
    public long getItemId(int position) {return position;}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.items_listview_scores, parent,false);
        }
        View vi = convertView;
        vi = inflater.inflate(R.layout.items_listview_scores,null);
        HashMap<String, String> scores = new HashMap<String, String>();
        scores = data.get(position);
        final TextView estadio  = (TextView)vi.findViewById(R.id.nameEstadio);
        final TextView nameHome       = (TextView)vi.findViewById(R.id.nameHome);
        final TextView nameVisitor    = (TextView)vi.findViewById(R.id.nameVis);
        final TextView fechaJuego = (TextView) vi.findViewById(R.id.fechaJuego);
        TextView carrerasVisitor= (TextView)vi.findViewById(R.id.cVis);
        TextView carrerasHome   = (TextView)vi.findViewById(R.id.cHome);
        TextView hitsHome       = (TextView)vi.findViewById(R.id.hHome);
        TextView hitsVis        = (TextView)vi.findViewById(R.id.hVis);
        final TextView erroresVisitor = (TextView)vi.findViewById(R.id.eVis);
        botonAgendar     = (Button)vi.findViewById(R.id.buttonAgendar);
        TextView erroresHome    = (TextView)vi.findViewById(R.id.eHome);
        TextView pbp_url        = (TextView)vi.findViewById(R.id.pbp_url);
        TextView codigoJuego    = (TextView)vi.findViewById(R.id.idJuego);
        final TextView hora           = (TextView)vi.findViewById(R.id.hora);
        TextView innings        = (TextView)vi.findViewById(R.id.innigns);
        ImageView logoHome      = (ImageView) vi.findViewById(R.id.imgHome);
        ImageView logovis       = (ImageView) vi.findViewById(R.id.imgVis);

        botonAgendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameStadio = estadio.getText().toString();
                String nameLocal = nameHome.getText().toString();
                String nameVis = nameVisitor.getText().toString();
                String horaJuego = hora.getText().toString();
                String fecha = fechaJuego.getText().toString();
                String fechaArray[] = fecha.split("/");
                diaAgenda = Integer.parseInt(fechaArray[0]);
                mesAgenda = Integer.parseInt(fechaArray[1])-1;
                anioAgenda = Integer.parseInt(fechaArray[2]);

                //calIntent.setType("vnd.android.cursor.item/event");
                Intent calIntent = new Intent(Intent.ACTION_EDIT)
                    .setData(CalendarContract.Events.CONTENT_URI)
                    .putExtra(CalendarContract.Events.TITLE, nameLocal+" VS "+nameVis)
                    .putExtra(CalendarContract.Events.EVENT_LOCATION,nameStadio)
                    .putExtra(CalendarContract.Events.DESCRIPTION, "Juego");

                GregorianCalendar calDate = new GregorianCalendar(anioAgenda,mesAgenda, diaAgenda);
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, calDate.getTimeInMillis());
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, calDate.getTimeInMillis());
                activity.startActivity(calIntent);

            }
        });

        /**
         * Getting Data*/

        String urlimageHome     = scores.get(Parse_calendar_dates.TAG_LOCAL_IMG);
        String urlimageVis      = scores.get(Parse_calendar_dates.TAG_VISITANTE_IMG);
        Glide.with(activity)
                .load(urlimageHome)
                .placeholder(R.drawable.placeholder)
                .into(logoHome);
        Glide.with(activity)
                .load(urlimageVis)
                .placeholder(R.drawable.placeholder)
                .into(logovis);
        String cHome = scores.get(Parse_calendar_dates.TAG_C);
        String cVis = scores.get(Parse_calendar_dates.TAG_C1);
        String hHome = scores.get(Parse_calendar_dates.TAG_H);
        String hVis = scores.get(Parse_calendar_dates.TAG_H1);
        String eHome = scores.get(Parse_calendar_dates.TAG_E);
        String eVis = scores.get(Parse_calendar_dates.TAG_E1);

        // Setting all values in listview
        estadio.setText(scores.get(Parse_calendar_dates.TAG_ESTADIO));
        nameHome.setText(scores.get(Parse_calendar_dates.TAG_LOCAL));
        fechaJuego.setText(scores.get(Parse_calendar_dates.TAG_FECHA));
        nameVisitor.setText(scores.get(Parse_calendar_dates.TAG_VISITANTE));
        pbp_url.setText(scores.get(Parse_calendar_dates.TAG_MINUTOAMINUTO));
        codigoJuego.setText(scores.get(Parse_calendar_dates.TAG_IDJUEGOS));
        carrerasHome.setText(cHome);
        carrerasVisitor.setText(cVis);
        hitsHome.setText(hHome);
        hitsVis.setText(hVis);
        erroresHome.setText(eHome);
        erroresVisitor.setText(eVis);
        hora.setText(scores.get(Parse_calendar_dates.TAG_HORA));
        innings.setText("Innings: "+scores.get(Parse_calendar_dates.TAG_INNING));
        return vi;
    }


}

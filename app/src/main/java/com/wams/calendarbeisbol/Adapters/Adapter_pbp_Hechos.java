package com.wams.calendarbeisbol.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wams.calendarbeisbol.Parse.Parse_pbp;
import com.wams.calendarbeisbol.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by WilliamO on 7/21/2016.
 */
public class Adapter_pbp_Hechos extends BaseAdapter {

    private Activity activity;
    private ArrayList<HashMap<String, String>> hechsPbp;
    private static LayoutInflater inflater = null;

    public Adapter_pbp_Hechos(Activity a, ArrayList<HashMap<String,String>> hechosPbp){
        this.activity = a;
        this.hechsPbp = hechosPbp;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public int getCount() {
        return hechsPbp.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.items_listview_pbp, parent,false);
        }

        View vi = convertView;

        vi = inflater.inflate(R.layout.items_listview_pbp,null);
        HashMap<String, String> juego = new HashMap<String, String>();
        juego = hechsPbp.get(position);

        TextView inning = (TextView) vi.findViewById(R.id.hechosInning);
        TextView accion = (TextView) vi.findViewById(R.id.hechosAccion);
        TextView hechos = (TextView)vi.findViewById(R.id.hechoshecho);

        inning.setText(juego.get(Parse_pbp.TAG_INNING_HECHOS));
        String parte = juego.get(Parse_pbp.TAG_PARTE);
        if(parte.equals("Alta")){
            parte="^";
        }else if(parte.equals("Baja")){
            parte="v";
        }
        accion.setText(parte);
        hechos.setText(juego.get(Parse_pbp.TAG_ACCION));

        return vi;
    }
}

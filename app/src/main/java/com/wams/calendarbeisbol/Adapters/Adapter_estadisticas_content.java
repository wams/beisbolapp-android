package com.wams.calendarbeisbol.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.wams.calendarbeisbol.R;

/**
 * Created by WilliamO on 9/3/2016.
 */
public class Adapter_estadisticas_content extends BaseAdapter {
    private Activity activity;
    private static LayoutInflater inflater = null;
    String[] nombreArray_estadisticas = {"J. Altuve","D. Pedroia","Y. Escobar","M. Betts","M. Trout","D. Ortiz","F. Lindor","M. Cabrera"};
    String[] averageArray_estadisticas= {".351",".323",".320",".320",".319",".313",".313",".309"};
    String[] imageArray_estadicticas  = {"http://elpuertomagallanero.net/wp-content/uploads/2015/04/josealtuve.png",
            "http://a.espncdn.com/combiner/i?img=/i/headshots/mlb/players/full/6393.png&w=350&h=254",
            "http://l2.yimg.com/bt/api/res/1.2/Lea4gfjeie5LsLngzg0YUg--/YXBwaWQ9eW5ld3NfbGVnbztmaT1maWxsO2g9MjMwO3E9NzU7dz0zNDU-/https://s.yimg.com/xe/i/us/sp/v/mlb_cutout/players_l/20160401/7938.png",
            "http://l.yimg.com/bt/api/res/1.2/jxZIy18Sjlm6mA9AhWv3jw--/YXBwaWQ9eW5ld3NfbGVnbztmaT1maWxsO2g9MjMwO3E9NzU7dz0zNDU-/https://s.yimg.com/xe/i/us/sp/v/mlb_cutout/players_l/20160401/9552.png",
            "http://67.media.tumblr.com/c2f787ee4efe2ff38660f65ae7f8475a/tumblr_inline_n5amgqupto1sbrjwg.png",
            "http://a.espncdn.com/combiner/i?img=/i/headshots/mlb/players/full/3748.png&w=350&h=254",
            "http://l2.yimg.com/bt/api/res/1.2/vzBdrvTgmBuSQwG8ddIq5A--/YXBwaWQ9eW5ld3NfbGVnbztmaT1maWxsO2g9MjMwO3E9NzU7dz0zNDU-/https://s.yimg.com/xe/i/us/sp/v/mlb_cutout/players_l/20160401/9116.png",
            "http://a.espncdn.com/combiner/i?img=/i/headshots/mlb/players/full/5544.png&w=350&h=254"};



    public Adapter_estadisticas_content(Activity a){
        activity = a;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return nombreArray_estadisticas.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.items_listview_estadisticas, parent,false);
        }
        View vi = convertView;
        vi = inflater.inflate(R.layout.items_listview_estadisticas,null);

        TextView nombre = (TextView)vi.findViewById(R.id.lbl_estadisticas_nombre);
        TextView average = (TextView)vi.findViewById(R.id.lbl_estadisticas_puntos);
        final ImageView imagen = (ImageView)vi.findViewById(R.id.image_estadisticas_jugador);

        nombre.setText(nombreArray_estadisticas[position]);
        average.setText(averageArray_estadisticas[position]);
        String urlimage = imageArray_estadicticas[position];

       /* Glide.with(activity)
                .load(urlimage)
                .placeholder(R.drawable.placeholder)
                .transform(new CircleTransform(context))
                .into(imagen); */

        Glide.with(activity).load(urlimage).asBitmap().centerCrop().into(new BitmapImageViewTarget(imagen) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imagen.setImageDrawable(circularBitmapDrawable);
            }
        });


        return vi;
    }
}

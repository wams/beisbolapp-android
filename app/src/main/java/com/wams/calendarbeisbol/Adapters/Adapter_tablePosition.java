package com.wams.calendarbeisbol.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wams.calendarbeisbol.Loader_img.ImageLoader;
import com.wams.calendarbeisbol.Parse.Parse_Posiciones;
import com.wams.calendarbeisbol.R;

import java.util.ArrayList;
import java.util.HashMap;



public class Adapter_tablePosition extends BaseAdapter {

    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater = null;
    public ImageLoader imageLoader;

    public Adapter_tablePosition(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data = d;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(activity.getApplicationContext());
        imageLoader.setImageDefault(R.drawable.placeholder);
        imageLoader.setImageSize(100);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_listview_position, parent,false);
        }
        View vi = convertView;

        vi = inflater.inflate(R.layout.item_listview_position,null);
        HashMap<String, String> juego = new HashMap<String, String>();
        juego = data.get(position);

        TextView nombre  = (TextView)vi.findViewById(R.id.POS_team);
        TextView juegosj = (TextView) vi.findViewById(R.id.POS_JJ);
        TextView juegosg = (TextView) vi.findViewById(R.id.POS_JG);
        TextView juegosp = (TextView) vi.findViewById(R.id.POS_JP);
        TextView diferencia = (TextView) vi.findViewById(R.id.POS_Dif);
        ImageView logo = (ImageView) vi.findViewById(R.id.POS_img);
        String urlimage = juego.get(Parse_Posiciones.TAG_IMG);

        // Setting all values in listview

        nombre.setText(juego.get(Parse_Posiciones.TAG_NAME));
        juegosj.setText(juego.get(Parse_Posiciones.TAG_JJ));
        juegosg.setText(juego.get(Parse_Posiciones.TAG_JG));
        juegosp.setText(juego.get(Parse_Posiciones.TAG_JP));
        diferencia.setText(juego.get(Parse_Posiciones.TAG_DIF));
        Glide.with(activity)
                .load(urlimage)
                .placeholder(R.drawable.placeholder)
                .into(logo);


        return vi;
    }

}

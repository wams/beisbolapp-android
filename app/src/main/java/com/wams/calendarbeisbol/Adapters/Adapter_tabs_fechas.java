package com.wams.calendarbeisbol.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.wams.calendarbeisbol.Libs.Utils;
import com.wams.calendarbeisbol.fragmentsJava.Fragment_ScoresView;

import org.json.JSONArray;


/**
 * Created by WilliamO on 6/23/2016.
 */
public class Adapter_tabs_fechas extends FragmentStatePagerAdapter {
    String [] dates;
    public JSONArray jsondata;

    public Adapter_tabs_fechas(FragmentManager fm, String[] data, JSONArray jsondata) {
        super(fm);
        this.dates = data;
        this.jsondata = jsondata;
    }
        @Override
        public Fragment getItem(int position)
        {
            Fragment_ScoresView fm = new Fragment_ScoresView();
            if(dates[0]=="" && jsondata==null){
                fm.setFecha(dates[position],jsondata);
                return fm;
            }else{
                fm.setFecha(dates[position],jsondata);
                return fm;
            }
        }
    @Override
        public int getCount() {
            return dates.length;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return  Utils.formateaFecha((String) this.dates[position], " d MMM");
        }

}


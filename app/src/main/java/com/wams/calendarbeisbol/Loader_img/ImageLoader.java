package com.wams.calendarbeisbol.Loader_img;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import com.wams.calendarbeisbol.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



public class ImageLoader {


	MemoryCache memoryCache=new MemoryCache();
	FileCache fileCache;
	int imgSize = 70;
	private int stub_id = R.drawable.ic_drawer;
	private int REQUIRED_SIZE = 0;

	private Map<ImageView, String> imageViews= Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
	ExecutorService executorService;

	public ImageLoader(Context context){
		fileCache=new FileCache(context);
		executorService= Executors.newFixedThreadPool(5);
	}


	public void DisplayImage(String url, ImageView imageView)
	{
		imageViews.put(imageView, url);
		Bitmap bitmap=memoryCache.get(url);
		if(bitmap!=null)
			imageView.setImageBitmap(bitmap);
		else
		{
			queuePhoto(url, imageView);
			imageView.setImageResource(stub_id);
		}
	}

	public int getImageDefault() {
		return stub_id;
	}

	public void setImageDefault(int Default) {
		stub_id = Default;
	}

	public int getImageSize() {
		return stub_id;
	}

	public void setImageSize(int size) {
		REQUIRED_SIZE = size;
	}	

	private void queuePhoto(String url, ImageView imageView)
	{
		PhotoToLoad p=new PhotoToLoad(url, imageView);
		executorService.submit(new PhotosLoader(p));
	}

	private Bitmap getBitmap(String url)
	{
		File f=fileCache.getFile(url);

		//from SD cache
		Bitmap b = decodeFileURL(f);
		if(b!=null)
			return b;

		//from web
		try {
			Bitmap bitmap=null;
			URL imageUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			InputStream is=conn.getInputStream();
			OutputStream os = new FileOutputStream(f);
			Utils.CopyStream(is, os);
			os.close();
			bitmap = decodeFileURL(f);
			return bitmap;
		} catch (Exception ex){
			ex.printStackTrace();
			return null;
		}
	}

	//decodes image and scales it to reduce memory consumption
	public Bitmap decodeFile(File f){
		try {
			Bitmap bitmap=memoryCache.get(f.getAbsolutePath());
			if(bitmap!=null){
				return bitmap;
			}else{
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(f), null, o);

				int scale = calculateInSampleSize(o, 200,150);

				//decode with inSampleSize
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				//	o2.inJustDecodeBounds = false;
				//o2.inPreferredConfig = Config.RGB_565;
				//o2.inDither = true;
				o2.inSampleSize=(int) scale;
				Bitmap bm = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
				memoryCache.put(f.getAbsolutePath(), bm);
				return bm;
			}
		} catch (FileNotFoundException e) {}
		return null;
	}

	public Bitmap decodeFileURL(File f){
		try {
			//decode image size
			Bitmap bitmap=memoryCache.get(f.getAbsolutePath());
			if(bitmap!=null){
				return bitmap;
			}else{

				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(f), null, o);

				int scale = calculateInSampleSize(o, 800,600);

				//decode with inSampleSize
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				//	o2.inJustDecodeBounds = false;
				//o2.inPreferredConfig = Config.RGB_565;
				//o2.inDither = true;
				o2.inSampleSize=(int) scale;
				Bitmap bm = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
				memoryCache.put(f.getAbsolutePath(), bm);
				return bm;
			}
		} catch (FileNotFoundException e) {}
		return null;
	}
	public Bitmap decodeFile(File f, int width, int heigth){
		try {
			Bitmap bitmap=memoryCache.get(f.getAbsolutePath());
			if(bitmap!=null){
				return bitmap;
			}else{
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(f), null, o);

				int scale = calculateInSampleSize(o, width,heigth);

				//decode with inSampleSize
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inJustDecodeBounds = false;
				o2.inPreferredConfig = Config.RGB_565;
				o2.inDither = true;
				o2.inSampleSize=(int) scale;
				Bitmap bm = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
				memoryCache.put(f.getAbsolutePath(), bm);
				return bm;
			}
		} catch (FileNotFoundException e) {}
		return null;
	}
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) >= reqHeight
					&& (halfWidth / inSampleSize) >= reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}



	//Task for the queue
	private class PhotoToLoad
	{
		public String url;
		public ImageView imageView;
		public PhotoToLoad(String u, ImageView i){
			url=u; 
			imageView=i;
		}
	}

	class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;
		PhotosLoader(PhotoToLoad photoToLoad){
			this.photoToLoad=photoToLoad;
		}

		@Override
		public void run() {
			if(imageViewReused(photoToLoad))
				return;
			Bitmap bmp=getBitmap(photoToLoad.url);
			memoryCache.put(photoToLoad.url, bmp);
			if(imageViewReused(photoToLoad))
				return;
			BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad);
			Activity a=(Activity)photoToLoad.imageView.getContext();
			a.runOnUiThread(bd);
		}
	}

	boolean imageViewReused(PhotoToLoad photoToLoad){
		String tag=imageViews.get(photoToLoad.imageView);
		if(tag==null || !tag.equals(photoToLoad.url))
			return true;
		return false;
	}

	//Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable
	{
		Bitmap bitmap;
		PhotoToLoad photoToLoad;
		public BitmapDisplayer(Bitmap b, PhotoToLoad p){bitmap=b;photoToLoad=p;}
		public void run()
		{
			if(imageViewReused(photoToLoad))
				return;
			if(bitmap!=null)
				photoToLoad.imageView.setImageBitmap(bitmap);
			else
				photoToLoad.imageView.setImageResource(stub_id);
		}
	}

	public void clearCache() {
		memoryCache.clear();
		fileCache.clear();
	}

}

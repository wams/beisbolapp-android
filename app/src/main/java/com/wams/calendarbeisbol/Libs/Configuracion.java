package com.wams.calendarbeisbol.Libs;

public class Configuracion {
    public static final String JSON_JUEGOS = "http://esinmuebles.com/apps/archivos_json/juegos.json";
    public static final String JSON_POSITIONS = "http://esinmuebles.com/apps/archivos_json/jsonposition.json";
    public static final String JSON_CALENDAR = "http://esinmuebles.com/apps/archivos_json/calendario.json";
    public static final String JSON_FELINAS = "http://esinmuebles.com/apps/archivos_json/chicasbeisbol_ios.json";
    public static final String JSON_ESTADIOS = "http://esinmuebles.com/apps/archivos_json/estadios.json";
}

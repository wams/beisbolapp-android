package com.wams.calendarbeisbol.Libs;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class RalewayMediumTextView extends TextView {
    private static Typeface sMaterialDesignIcons;

    public RalewayMediumTextView(Context context) {
        this(context, null);
    }

    public RalewayMediumTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RalewayMediumTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            setTypeface();
        }
    }

    private void setTypeface() {
        if (sMaterialDesignIcons == null) {
            sMaterialDesignIcons = Typeface.createFromAsset(getContext().getAssets(), "fonts/oswald-light.ttf");
        }
        setTypeface(sMaterialDesignIcons);
    }
}

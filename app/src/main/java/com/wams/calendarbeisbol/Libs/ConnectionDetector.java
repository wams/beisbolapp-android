package com.wams.calendarbeisbol.Libs;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionDetector {

	private Context _context;

	public ConnectionDetector(Context context){
		this._context = context;
	}

	/**
	 * Checking for all possible internet providers
	 * **/
	/*public boolean isConnectingToInternet(){

		ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
					{
						return true;
					}

		}
		return false;
	}*/

	public boolean hasInternet(Activity a) { 
		boolean hasConnectedWifi = false; 
		boolean hasConnectedMobile = false; 

		ConnectivityManager cm = (ConnectivityManager) a.getSystemService(Context.CONNECTIVITY_SERVICE); 
		NetworkInfo[] netInfo = cm.getAllNetworkInfo(); 
		for (NetworkInfo ni : netInfo) { 
			if (ni.getTypeName().equalsIgnoreCase("wifi")) 
				if (ni.isConnected()) 
					hasConnectedWifi = true; 
			if (ni.getTypeName().equalsIgnoreCase("mobile")) 
				if (ni.isConnected()) 
					hasConnectedMobile = true; 
		} 
		return hasConnectedWifi || hasConnectedMobile; 
	}
}

package com.wams.calendarbeisbol.Libs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by WilliamO on 7/7/2016.
 */
public class Utils {

    public static String formateaFecha(String date, String formato) {
        Date convertedDate = new Date();
        try {
            convertedDate = new SimpleDateFormat("dd/MM/yyyy").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = new SimpleDateFormat(formato).format(convertedDate);
        System.out.println("Current time => " + formattedDate);
        return formattedDate;
    }
}

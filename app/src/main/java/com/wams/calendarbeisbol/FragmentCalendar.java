package com.wams.calendarbeisbol;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.wams.calendarbeisbol.DecoratosCalendar.EventDecorator;
import com.wams.calendarbeisbol.DecoratosCalendar.HighlightWeekendsDecorator;
import com.wams.calendarbeisbol.DecoratosCalendar.MySelectorDecorator;
import com.wams.calendarbeisbol.DecoratosCalendar.OneDayDecorator;
import com.wams.calendarbeisbol.libsCalendar.CalendarDay;
import com.wams.calendarbeisbol.libsCalendar.DayViewDecorator;
import com.wams.calendarbeisbol.libsCalendar.DayViewFacade;
import com.wams.calendarbeisbol.libsCalendar.MaterialCalendarView;
import com.wams.calendarbeisbol.libsCalendar.OnDateSelectedListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executors;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by WilliamO on 7/12/2016.
 */
public class FragmentCalendar extends AppCompatActivity implements OnDateSelectedListener,Serializable {
    private final OneDayDecorator oneDayDecorator = new OneDayDecorator();
    @Bind(R.id.calendarView)
    MaterialCalendarView widget;
    public ArrayList<String> fechasScores;
    String  datosJson;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_calendario);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        Bundle b = getIntent().getExtras();
        if(b!=null){
            fechasScores = b.getStringArrayList("fechasScores");
            datosJson = b.getString("jsonData");
        }

        /*************************************************************************/
        ButterKnife.bind(this);
        widget.setOnDateChangedListener(this);
        widget.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        Calendar instance = Calendar.getInstance();
        widget.setSelectedDate(instance.getTime());
        Calendar instance1 = Calendar.getInstance();
        instance1.set(instance1.get(Calendar.YEAR)-1, Calendar.JANUARY, 1);
        Calendar instance2 = Calendar.getInstance();
        instance2.set(instance2.get(Calendar.YEAR)+1, Calendar.DECEMBER, 31);
        widget.state().edit()
                .setMinimumDate(instance1.getTime())
                .setMaximumDate(instance2.getTime())
                .commit();
        widget.addDecorators(new MySelectorDecorator(this) , new HighlightWeekendsDecorator(), oneDayDecorator);
        widget.addDecorator(new PrimeDayDisableDecorator());
        widget.addDecorator(new EnableOneToTenDecorator()) ;

        new ApiSimulator().executeOnExecutor(Executors.newSingleThreadExecutor());
    }
    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        //If you change a decorate, you need to invalidate decorators
        int indexTabsCalendar=0;
        int auxMes = 0;
        String stringauxMes="";
        String stringauxDia="";
        oneDayDecorator.setDate(date.getDate());
        widget.invalidateDecorators();

        int dia= date.getDay();
        int anio = date.getYear();
        int mes = date.getMonth();

        if(dia <=9){
            stringauxDia = "0"+dia;
        }else{
            stringauxDia = Integer.toString(dia);
        }

        if(mes <9){
             int m =mes+1;
             stringauxMes = "0"+m;
        }else{
            int m = mes +1;
            stringauxMes = Integer.toString(m);
        }
        String fechaCalendar = stringauxDia+"/"+ stringauxMes+"/"+anio;

        for(int i=0; i<fechasScores.size();i++){
            if(fechaCalendar.equals(fechasScores.get(i))){
                indexTabsCalendar = i ;
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("dateCalendario",indexTabsCalendar);
                //intent.putExtra("datosJsonScores",datosJson);
                startActivity(intent);
            }
        }
    }
    private class ApiSimulator extends AsyncTask<Void, Void, List<CalendarDay>> {

        ArrayList<CalendarDay> scores = new ArrayList<>();
        @Override
        protected List<CalendarDay> doInBackground(@NonNull Void... voids) {

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(fechasScores!=null){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -12);

            for(int i=0; i<fechasScores.size();i++){
                String fechaAux = fechasScores.get(i);
                String[] fechaArray = fechaAux.split("/");
                int anio = Integer.parseInt(fechaArray[2]);
                int mes  = Integer.parseInt(fechaArray[1])-1;
                int dia  = Integer.parseInt(fechaArray[0]);
                CalendarDay day = CalendarDay.from(anio,mes,dia);
                scores.add(day);
             }
            }
            return scores;
        }
        @Override
        protected void onPostExecute(@NonNull List<CalendarDay> calendarDays) {

            super.onPostExecute(calendarDays);
            if (isFinishing()) {
                return;
            }
            widget.addDecorator(new EventDecorator(Color.RED, scores));
        }
    }
    private static class PrimeDayDisableDecorator implements DayViewDecorator {

        @Override
        public boolean shouldDecorate(com.wams.calendarbeisbol.libsCalendar.CalendarDay day) {

            return PRIME_TABLE[day.getDay()];
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setDaysDisabled(true);
        }
        private static boolean[] PRIME_TABLE = {
                true,  // 0?
                true,
                true, // 2
                true, // 3
                true,
                true, // 5
                true,
                true, // 7
                true,
                true,
                true,
                true, // 11
                true,
                true, // 13
                true,
                true,
                true,
                true, // 17
                true,
                true, // 19
                true,
                true,
                true,
                true, // 23
                true,
                true,
                true,
                true,
                true,
                true, // 29
                true,
                true, // 31
                true,
                true,
                true, //PADDING
        };
    }
    private static class EnableOneToTenDecorator implements com.wams.calendarbeisbol.libsCalendar.DayViewDecorator {

        @Override
        public boolean shouldDecorate(com.wams.calendarbeisbol.libsCalendar.CalendarDay day) {

            return false;
        }

        @Override
        public void decorate(com.wams.calendarbeisbol.libsCalendar.DayViewFacade view) {
            view.setDaysDisabled(false);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
